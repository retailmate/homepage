package com.cognizant.retailmate.screen;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.retailmate.screen.Network.VolleyHelper;
import com.cognizant.retailmate.screen.Network.VolleyRequest;
import com.cognizant.retailmate.screen.adapter.CustomPagerAdapter;
import com.cognizant.retailmate.screen.adapter.RecyclerAdapter;
import com.cognizant.retailmate.screen.adapter.ViewPagerAdapter;
import com.cognizant.retailmate.screen.models.Category1;
import com.cognizant.retailmate.screen.models.Category2;
import com.cognizant.retailmate.screen.models.Category3;
import com.cognizant.retailmate.screen.models.ProductModel;
import com.cognizant.retailmate.screen.models.RecommendationModel;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomePageActivity extends AppCompatActivity {

    private ViewPager mViewPager;
    private CustomPagerAdapter mCustomPagerAdapter;
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    CollapsingToolbarLayout collapsingToolbarLayout;
    AppBarLayout appBarLayout;

    ImageView home_i, bag_i, plus_more, chat_i, profile_i;
    View home_v, bag_v, chat_v, profile_v;
    LinearLayout bottom_nav;
    View bottom_view;
    LinearLayout home, bag, chat, profile;

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;

    Animation slideUpAnimation, slideDownAnimation;

    int[] mResources = {
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four
    };

    List<ProductModel> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        slideDownAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.CollapsingToolbarLayout);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        bottom_view = findViewById(R.id.bottom_view);
        bottom_nav = (LinearLayout) findViewById(R.id.bottom_navigation);

        home = (LinearLayout) findViewById(R.id.home);
        bag = (LinearLayout) findViewById(R.id.bag);
        chat = (LinearLayout) findViewById(R.id.chat);
        profile = (LinearLayout) findViewById(R.id.profile);

        plus_more = (ImageView) findViewById(R.id.plus);

        home_v = findViewById(R.id.home_view);
        home_i = (ImageView) findViewById(R.id.home_icon);

        bag_v = findViewById(R.id.bag_view);
        bag_i = (ImageView) findViewById(R.id.bag_icon);

        chat_v = findViewById(R.id.chat_view);
        chat_i = (ImageView) findViewById(R.id.chat_icon);

        profile_v = findViewById(R.id.profile_view);
        profile_i = (ImageView) findViewById(R.id.profile_icon);

        home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
        home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green));

        bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

        chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

        profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
        profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("##$$", String.valueOf(collapsingToolbarLayout.getHeight() + verticalOffset) + " ** " + String.valueOf(2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)));
                if (collapsingToolbarLayout.getHeight() + verticalOffset < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
                    Log.e("##$$", "Closed");
                    bottom_view.startAnimation(slideDownAnimation);
                    bottom_view.setVisibility(View.GONE);

                } else {
                    Log.e("##$$", "Opened");
                    bottom_view.setVisibility(View.VISIBLE);
//                    bottom_view.startAnimation(slideUpAnimation);
                }
            }
        });

//        Log.e("@@##",String.valueOf(mResources.length));

        mViewPager = (ViewPager) findViewById(R.id.pager);
        TabLayout tabLayout_dots = (TabLayout) findViewById(R.id.tabDots);


        mCustomPagerAdapter = new CustomPagerAdapter(this, mResources);
        mViewPager.setAdapter(mCustomPagerAdapter);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.products_container);
        recyclerView.setLayoutManager(layoutManager);

        populateData();
        recommendationApiCall();

        recyclerAdapter = new RecyclerAdapter(getApplicationContext(), productList);
        recyclerView.setAdapter(recyclerAdapter);


        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addFragments(new Category1(), "Catogory 1");
        viewPagerAdapter.addFragments(new Category2(), "Category 2");
        viewPagerAdapter.addFragments(new Category3(), "Category 3");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout_dots.setupWithViewPager(mViewPager);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
            }
        });


        bag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }
        });

        chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                bag_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                profile_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                home_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                bag_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                bag_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                chat_v.setBackgroundColor(getResources().getColor(R.color.transparent));
                chat_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.transparent));

                profile_v.setBackgroundColor(getResources().getColor(R.color.green));
                profile_i.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.green));
            }
        });

    }

    private void populateData() {

        ProductModel model = new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Camera");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 124.0);
        productList.add(model);

        model = new ProductModel();
        model.setName("Tripod");
        model.setCategory("Camera Accessories");
        model.setPrice((float) 12.0);
        productList.add(model);

    }

    void recommendationApiCall() {

        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("UserID", "004021"));

        VolleyRequest<RecommendationModel> recommendationModelVolleyRequest =
                new VolleyRequest<RecommendationModel>(Request.Method.POST, Constants.RECOMMENDATION_API, RecommendationModel.class, headers, params.toString(),

                        new com.android.volley.Response.Listener<RecommendationModel>() {
                            @Override
                            public void onResponse(RecommendationModel response) {
                                Toast.makeText(HomePageActivity.this, "Your Recommendations \n" + response.getValue(), Toast.LENGTH_SHORT).show();

                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(HomePageActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }
}
