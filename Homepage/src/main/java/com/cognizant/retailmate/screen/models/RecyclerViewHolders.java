package com.cognizant.retailmate.screen.models;

/**
 * Created by 599584 on 2/17/2017.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailmate.screen.R;

public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView productname;
    public ImageView Photo;

    public RecyclerViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        this.productname = (TextView)itemView.findViewById(R.id.productname);
        this.Photo = (ImageView)itemView.findViewById(R.id.photo);
    }

    @Override
    public void onClick(View view) {
    }
}